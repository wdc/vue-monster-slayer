const LOG_TYPE_MONSTER = 'monster-turn';
const LOG_TYPE_PLAYER = 'player-turn';

function random(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

var app = new Vue({
  el: '#app',
  data: {
    gameStart: false,
    healthPlayer: 100,
    healthMonster: 100,
    logs: [],
  },
  computed: {
    statusPlayer() {
      if (healthPlayer <= 20) {
        return 'red';
      } else if (healthPlayer > 20 && healthPlayer <= 60) {
        return 'orange';
      } else if (healthPlayer > 60) {
        return 'green';
      }
    },
    statusMonster() {
      if (healthMonster <= 20) {
        return 'red';
      } else if (healthMonster > 20 && healthMonster <= 60) {
        return 'orange';
      } else if (healthMonster > 60) {
        return 'green';
      }
    },
  },
  methods: {
    startGame() {
      console.log('start new game');
    },
    attack() {
      const attackDamage = random(5, 10);

      this.monsterMove();
      this.healthMonster -= attackDamage;

      this.log(LOG_TYPE_PLAYER, `player hits monster for ${attackDamage}`);
    },
    specialAttack() {
      const attackDamage = random(10, 20);

      this.monsterMove();
      this.healthMonster -= attackDamage;

      this.log(LOG_TYPE_PLAYER, `player hits monster for ${attackDamage}`);
    },
    heal() {
      const healAmount = random(5, 10);

      this.monsterMove();
      this.healthPlayer += healAmount;

      this.log(LOG_TYPE_PLAYER, `player heals himself for ${healAmount}`);
    },
    giveUp() {
      console.log('give up');
    },
    monsterMove() {
      let attackDamage = random(5, 10);
      this.healthPlayer -= attackDamage;

      this.log(LOG_TYPE_MONSTER, `monster hits player for ${attackDamage}`);
    },
    log(type, text) {
      const obj = {
        type: type,
        text: text,
      };
      this.logs.push(obj);
    },
  },
});
